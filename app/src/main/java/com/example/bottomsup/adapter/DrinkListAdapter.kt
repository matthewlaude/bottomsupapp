package com.example.bottomsup.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.bottomsup.databinding.ItemDrinkBinding
import com.example.bottomsup.model.response.DrinkDetailsDTO
import com.example.bottomsup.model.response.DrinkListDTO
import com.example.bottomsup.view.drink.DrinkListFragment
import com.example.bottomsup.view.drink.DrinkListFragmentDirections
import com.squareup.picasso.Picasso

class DrinkListAdapter : RecyclerView.Adapter<DrinkListAdapter.DrinksViewHolder>() {

    private var drinks = listOf<DrinkListDTO.Drink>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrinksViewHolder {
        return DrinksViewHolder.getInstance(parent)
    }

    override fun onBindViewHolder(holder: DrinksViewHolder, position: Int) {
        holder.bindDrink(drinks[position])
    }

    override fun getItemCount(): Int = drinks.size

    fun addDrinks(drinks: List<DrinkListDTO.Drink>) {
        this.drinks = drinks
        notifyDataSetChanged()
    }

    class DrinksViewHolder(
        private val binding: ItemDrinkBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bindDrink(drink: DrinkListDTO.Drink) = with(binding) {
            tvItemDrink.text = drink.strDrink
            Picasso.get().load(drink.strDrinkThumb).into(ivDrink)
            tvItemDrink.setOnClickListener {
                it.findNavController()
                    .navigate(DrinkListFragmentDirections.actionDrinkListFragmentToDetailsFragment(drink.idDrink))
            }
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemDrinkBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { DrinksViewHolder(it) }
        }
    }
}