package com.example.bottomsup.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.bottomsup.databinding.ItemCategoryBinding
import com.example.bottomsup.model.response.CategoryDTO
import com.example.bottomsup.view.category.CategoryFragmentDirections

class CategoryAdapter : RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {

    private var categories = listOf<CategoryDTO.CategoryItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        return CategoryViewHolder.getInstance(parent)
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        holder.bindCategory(categories[position])
    }

    override fun getItemCount(): Int {
        return categories.size
    }

    fun addCategories(categories: CategoryDTO) {
        this.categories = categories.categoryItems
        notifyDataSetChanged()
    }

    class CategoryViewHolder(
        private val binding: ItemCategoryBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bindCategory(category: CategoryDTO.CategoryItem) = with(binding) {
            tvItemCategory.text = category.strCategory
            binding.tvItemCategory.setOnClickListener {
                it.findNavController().navigate(
                    CategoryFragmentDirections.goToDrinkListFragment(category.strCategory)
                )
            }
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemCategoryBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { CategoryViewHolder(it) }
        }
    }

}