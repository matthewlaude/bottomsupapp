package com.example.bottomsup.viewmodel

import android.util.Log
import androidx.lifecycle.*
import com.example.bottomsup.model.BottomsUpRepo
import com.example.bottomsup.model.response.DrinkListDTO
import com.example.bottomsup.view.drink.DrinkListState
import kotlinx.coroutines.launch

class DrinkListViewModel : ViewModel() {

    private val repo by lazy { BottomsUpRepo }

    private val _state = MutableLiveData<DrinkListState>()
    val state: LiveData<DrinkListState> get() = _state

    private val _stateList = MutableLiveData<DrinkListDTO>()
    val stateList: LiveData<DrinkListDTO> get() = _stateList

    fun getDrink(category: String) {
        viewModelScope.launch {
            val drinkListDTO = repo.getCategoryDrinks(category) // change val to "drink"?
            _state.value = DrinkListState(drinkCategories = drinkListDTO.drinks) // change to "_stringList"?

            _stateList.value = drinkListDTO
        }
    }

//    val state: LiveData<DrinkListState> = liveData {
//        emit(DrinkListState(isLoading = true))
//        viewModelScope.launch {
//            val categoryDrinks = repo.getCategoryDrinks(category)
//            emit(DrinkListState(drinks = categoryDrinks.drinks))
//        }
//    }
}