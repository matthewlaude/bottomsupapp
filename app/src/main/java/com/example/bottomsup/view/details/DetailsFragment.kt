package com.example.bottomsup.view.details

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.bottomsup.R
import com.example.bottomsup.adapter.DetailAdapter
import com.example.bottomsup.databinding.FragmentDetailsBinding
import com.example.bottomsup.viewmodel.DetailViewModel

class DetailsFragment : Fragment() {

    private var _binding: FragmentDetailsBinding? = null
    private val binding get() = _binding!!
    private val detailViewModel by viewModels<DetailViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDetailsBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val drinkId = arguments?.getString("drinkId")

        with(binding.rvDetails) {
            layoutManager = LinearLayoutManager(this.context)
            adapter = DetailAdapter().apply {
                with(detailViewModel) {
                    if (drinkId != null) {
                        getDetails(drinkId)
                    }
                    stateList.observe(viewLifecycleOwner) {
                        addDetails(it.drinks)
                    }
                }
            }
        }
    }

}