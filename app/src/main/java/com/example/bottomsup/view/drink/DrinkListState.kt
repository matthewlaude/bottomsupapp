package com.example.bottomsup.view.drink

import com.example.bottomsup.model.response.DrinkListDTO

data class DrinkListState(
//    val isLoading: Boolean = false,
    val drinkCategories: List<DrinkListDTO.Drink> = emptyList()
)