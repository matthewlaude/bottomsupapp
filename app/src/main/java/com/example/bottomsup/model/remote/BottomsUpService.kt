package com.example.bottomsup.model.remote

import com.example.bottomsup.model.response.CategoryDTO
import com.example.bottomsup.model.response.DrinkListDTO
import com.example.bottomsup.model.response.DrinkDetailsDTO
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Query

interface BottomsUpService {

    companion object {
        private const val BASE_URL = "https://www.thecocktaildb.com"
        private const val QUERY_CATEGORY = "c"

        fun getInstance(): BottomsUpService {
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            return retrofit.create()
        }
    }

    @GET("/api/json/v1/1/list.php")
    suspend fun getCategories(@Query(QUERY_CATEGORY) type: String = "list"): CategoryDTO

    @GET("/api/json/v1/1/filter.php")
    suspend fun getCategoryDrinks(@Query(QUERY_CATEGORY) category: String): DrinkListDTO
//    @GET("/api/json/v1/1/filter.php")
//    suspend fun getCategoryDrinks(@Query(QUERY_CATEGORY) type: String = "drink"): DrinkListDTO

    @GET("/api/json/v1/1/lookup.php")
    suspend fun getDrinkDetails(@Query("i") drinkId: String): DrinkDetailsDTO
}