package com.example.bottomsup.model

import com.example.bottomsup.model.remote.BottomsUpService
import com.example.bottomsup.model.response.CategoryDTO
import com.example.bottomsup.model.response.DrinkDetailsDTO
import com.example.bottomsup.model.response.DrinkListDTO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object BottomsUpRepo {

    private val bottomsUpService: BottomsUpService by lazy { BottomsUpService.getInstance() }

    suspend fun getCategories(): CategoryDTO = withContext(Dispatchers.IO) {
        bottomsUpService.getCategories()
    }

//    suspend fun getCategoryDrinks(category: String): DrinkListDTO =
//        withContext(Dispatchers.IO) {
//            bottomsUpService.getCategoryDrinks(category)
//        }
    suspend fun getCategoryDrinks(category: String): DrinkListDTO =
        withContext(Dispatchers.IO) {
            bottomsUpService.getCategoryDrinks(category)
        }

    suspend fun getDrinkDetails(drinkId: String): DrinkDetailsDTO = withContext(Dispatchers.IO) {
        bottomsUpService.getDrinkDetails(drinkId)
    }
}